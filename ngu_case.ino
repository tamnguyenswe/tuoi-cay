#include "SIM900.h"

#include <SoftwareSerial.h>

#include "sms.h"

#include <avr/interrupt.h>
#define ON 1
#define OFF 0

#define PHUT 229 // 1 phut = 229 timer overflow
//PIN 8 = bom 1, PIN 9 = bom 2
#define BOM_1 8
#define BOM_2 9

SMSGSM sms;
boolean started = false; //trạng thái modul sim 
char smstext[160]; // nội dung tin nhắn 
char number[20]; // số điện thoại format theo định dạng quốc tế
char info_1_15 = 0;
char info_2_15 = 0;
char info_1_30 = 0;
char info_2_30 = 0;
char p0 = 0;
char p1 = 0;
char p2 = 0;
char p3 = 0;
int bom_1_15 = 0;
int bom_1_30 = 0;
int bom_2_15 = 0;
int bom_2_30 = 0;
char bom_1_155 = 0;
char bom_1_300 = 0;
char bom_2_155 = 0;
char bom_2_300 = 0;
int gio1 = 0;
int gio2 = 0;
char offb1 = 0;
char offb2 = 0;


int * timer_voi_p = new int[5]; //timer_voi[1] = thoi gian bat con lai cua voi 1, = 0 thi tat voi (tinh theo phut)
//=======================

void setup() {
    Serial.begin(9600);
    pinMode(BOM_1, OUTPUT);
    pinMode(BOM_2, OUTPUT);
    digitalWrite(BOM_1, LOW);
    digitalWrite(BOM_2, LOW);
    cli(); // tắt ngắt toàn cục
    /* Reset Timer/Counter1 */
    TCCR1A = 0;
    TCCR1B = 0;
    TIMSK1 = 0;
    /* Setup Timer/Counter1 */
    TCCR1B |= (1 << CS11) | (1 << CS10); // prescale = 64
    TCNT1 = 0;
    TIMSK1 = (1 << TOIE1); // Overflow interrupt enable 
    sei();
    if (gsm.begin(2400)) {
        Serial.println("\nstatus=READY");
        started = true;
    } else
        Serial.println("\nstatus=IDLE");
}


void loop() {
    if (bom_1_155 == ON) {
        sms.SendSMS(number, "da tat bom 1 sau 15p");
        bom_1_155 = OFF;
    }

    if (bom_2_155 == ON) {
        sms.SendSMS(number, "da tat bom 2 sau 15p");
        bom_2_155 = OFF;
    }

    if (bom_1_300 == ON) {
        sms.SendSMS(number, "da tat bom 1 sau 30p");
        bom_1_300 = OFF;
    }

    if (bom_2_300 == ON) {
        sms.SendSMS(number, "da tat bom 2 sau 30p");
        bom_2_300 = OFF;
    }

    if (offb1 == ON) {
        sms.SendSMS(number, "da tat bom 1 sau 1h");
        offb1 = OFF;
    }

    if (offb2 == ON) {
        sms.SendSMS(number, "da tat bom 2 sau 1h");
        offb2 = OFF;
    }

    if (started) {
        int pos;
        pos = sms.IsSMSPresent(SMS_UNREAD);
        if (pos) {
            if (sms.GetSMS(pos, number, smstext, 160)) {
                if (strcmp(smstext, "*1on#") == 0) {
                    gio1 = 0;
                    digitalWrite(BOM_1, HIGH);
                }
                
                if (strcmp(smstext, "*2on#") == 0) {
                    gio2 = 0;
                    digitalWrite(BOM_2, HIGH);
                }

                if (strcmp(smstext, "*1on15#") == 0) {
                    p0 = 1;
                    gio1 = 0;
                    digitalWrite(BOM_1, HIGH);
                    info_1_15 = 1;
                    bom_1_30 = 0;
                    p2 = 0;
                    info_1_30 = 0;
                    if (digitalRead(BOM_1) == 1) {
                        sms.SendSMS(number, "bat bom 1 15p");
                    }
                }
                if (strcmp(smstext, "*2on15#") == 0) {
                    p1 = 1;
                    gio2 = 0;
                    digitalWrite(BOM_2, HIGH);
                    info_2_15 = 1;
                    bom_2_30 = 0;
                    p3 = 0;
                    info_2_30 = 0;
                    if (digitalRead(BOM_2) == 1) {
                        sms.SendSMS(number, "bat bom 2 15p");
                    }
                }
                if (strcmp(smstext, "*1on30#") == 0) {
                    p2 = 1;
                    gio1 = 0;
                    digitalWrite(BOM_1, HIGH);
                    info_1_30 = 1;
                    bom_1_15 = 0;
                    p0 = 0;
                    info_1_15 = 0;
                    if (digitalRead(BOM_1) == 1) {
                        sms.SendSMS(number, "bat bom 1 30p");
                    }
                }
                if (strcmp(smstext, "*2on30#") == 0) {
                    p3 = 1;
                    gio2 = 0;
                    digitalWrite(BOM_2, HIGH);
                    info_2_30 = 1;
                    bom_2_15 = 0;
                    p1 = 0;
                    info_2_15 = 0;
                    if (digitalRead(BOM_2) == 1) {
                        sms.SendSMS(number, "bat bom 2 30p");
                    }
                }
                if (strcmp(smstext, "*1off#") == 0) {
                    gio1 = 0;
                    digitalWrite(BOM_1, LOW);
                    if (digitalRead(BOM_1) == 0) {
                        sms.SendSMS(number, "bom 1 tat");
                    }
                }
                if (strcmp(smstext, "*2off#") == 0) {
                    gio2 = 0;
                    digitalWrite(BOM_2, LOW);
                    if (digitalRead(BOM_2) == 0) {
                        sms.SendSMS(number, "bom 2 tat");
                    }
                }
                if (strcmp(smstext, "*off#") == 0) {
                    gio1 = 0;
                    gio2 = 0;
                    digitalWrite(BOM_1, LOW);
                    digitalWrite(BOM_2, LOW);
                    if ((digitalRead(BOM_1) == 0) && (digitalRead(BOM_2) == 0)) {
                        sms.SendSMS(number, "2 bom tat");
                    }
                }
                if (strcmp(smstext, "*on#") == 0) {
                    gio1 = 0;
                    gio2 = 0;
                    digitalWrite(8, HIGH);
                    digitalWrite(9, HIGH);
                    if ((digitalRead(8) == 1) && (digitalRead(9) == 1)) {
                        sms.SendSMS(number, "2 bom bat");
                    }
                }
                if (strcmp((smstext), "*info#") == 0) {
                    if ((digitalRead(8) == 0) && 
                        (digitalRead(9) == 0)) {
                        sms.SendSMS(number, "2 bom tat");
                    }
                    if ((digitalRead(8) == 1) &&
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "2 bom bat");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 1) && 
                        (info_2_15 == 1) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom1 che do 15p, bom2 che do 15p");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 1) && 
                        (info_2_30 == 1)) {
                        sms.SendSMS(number, "bom1 che do 30p, bom2 che do 30p");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 1) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 1)) {
                        sms.SendSMS(number, "bom1 che do 15p, bom2 che do 30p");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 1) && 
                        (info_1_30 == 1) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom1 che do 30p, bom2 che do 15p");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 1) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom1 che do 15, bom2 bat");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 1) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom1 bat, bom2 che do 15");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 1) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom1 che do 30, bom2 bat");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 1)) {
                        sms.SendSMS(number, "bom1 bat, bom2 che do 30");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 0) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom 1 bat, bom 2 tat");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 0) && 
                        (info_1_15 == 1) && 
                        (info_1_30 == 0)) {
                        sms.SendSMS(number, "bom1 che do 15p, bom 2 tat");
                    }
                    if ((digitalRead(8) == 1) && 
                        (digitalRead(9) == 0) && 
                        (info_1_15 == 0) && 
                        (info_1_30 == 1)) {
                        sms.SendSMS(number, "bom1 che do 30p, bom 2 tat");
                    }
                    if ((digitalRead(8) == 0) && 
                        (digitalRead(9) == 1) && 
                        (info_2_15 == 1) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom 1 tat, bom2 che do 15p");
                    }
                    if ((digitalRead(8) == 0) && 
                        (digitalRead(9) == 1) && 
                        (info_2_15 == 0) && 
                        (info_2_30 == 1)) {
                        sms.SendSMS(number, "bom 1 tat, bom2 che do 30p");
                    }
                    if ((digitalRead(8) == 0) && 
                        (digitalRead(9) == 1) && 
                        (info_1_15 == 0) && 
                        (info_2_15 == 0) && 
                        (info_1_30 == 0) && 
                        (info_2_30 == 0)) {
                        sms.SendSMS(number, "bom 1 tat, bom 2 bat");
                    }
                }
                sms.DeleteSMS(pos);
            }
        }
        delay(1000);
    } else Serial.println("Offline");
}


ISR(TIMER1_OVF_vect) {
    TCNT1 = 0;

    for (int i = 1; i <= 4; i++) {
        if (timer_voi_p[i] > 0) { //neu voi nao dang hen gio thi giam bo dem di
            timer_voi_p[i]--;
        }

        if (timer_voi_p[i] == 0) { // neu voi nao dem lui ve 0..
            tat(i);                // .. thi tat voi day di
        }
    }




    if (p0 == 1) {
        bom_1_15++;
    }
    if (bom_1_15 == (15 * PHUT)) { //3435
        digitalWrite(8, LOW);
        bom_1_155 = 1;
        p0 = 0;
        bom_1_15 = 0;
        info_1_15 = 0;
    }

    if (p1 == 1) {
        bom_2_15++;
    }
    if (bom_2_15 == (15 * PHUT)) {//3435
        digitalWrite(9, LOW);
        bom_2_155 = 1;
        p1 = 0;
        bom_2_15 = 0;
        info_2_15 = 0;
    }
    if (p2 == 1) {
        bom_1_30++;
    }
    if (bom_1_30 == (30 * PHUT)) {//6870
        digitalWrite(8, LOW);
        bom_1_300 = 1;
        p2 = 0;
        bom_1_30 = 0;
        info_1_30 = 0;
    }
    if (p3 == 1) {
        bom_2_30++;
    }

    if (bom_2_30 == (30 * PHUT)) {//6870
        digitalWrite(9, LOW);
        bom_2_300 = 1;
        p3 = 0;
        bom_2_30 = 0;
        info_2_30 = 0;
    }

    if (digitalRead(8) == 1) {// CHU Y 0 OR 1 BAT                          // SUA THEO CHAN DUNG 
        gio1++;
        if (gio1 == (60 * PHUT)) {// KIEM TRA 1 TIENG 13700
            gio1 = 0;
            offb1 = 1;
            digitalWrite(8, LOW);
        }
    }

    if (digitalRead(0) == 0) {
        gio1 = 0;
    }

    if (digitalRead(9) == 1) { // CHU Y 0 OR 1 BAT                          // SUA THEO CHAN DUNG 
        gio2++;
        if (gio2 == 13700) {
            gio2 = 0;
            offb2 = 1;
            digitalWrite(9, LOW);
        }
        if (digitalRead(9) == 0) {
            gio2 = 0;
        }
    }
}